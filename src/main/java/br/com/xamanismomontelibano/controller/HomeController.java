package br.com.xamanismomontelibano.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author s2it_dsilveira
 * @version $Revision: $<br/>
 * $Id: $
 * @since 29/01/18 18:50
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    public String index () {
        return "index";
    }

    @RequestMapping("/home")
    public String home () {
        return "index";
    }

    @RequestMapping("/teste")
    public String teste () {
        return "index";
    }

    @RequestMapping("/ola")
    public String ola () {
        return "index";
    }

}
